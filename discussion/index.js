console.log("hello world");

// Assignment Operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);

assignmentNumber +=2;
console.log(assignmentNumber);

// Subtraction/Multiplication/division assignment operator (-=, *=, /=)
assignmentNumber -=2;
assignmentNumber *=2;
assignmentNumber /=2;

console.log(assignmentNumber);

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of MDAS operation: " + mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of PEMDAS operation: " + pemdas);

let z = 1;
++z;
console.log(z);
z++;
console.log(z);

console.log(z++);
console.log(z);

console.log(++z);


let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);
console.log(typeof numE);


let juan = 3
console.log("Equality Operator");
console.log(1 == 1);//true
console.log(1 == 2);//false
console.log(1 == '1');//true
console.log(0 == false);//true
console.log('juan' == 'JUAN');//false - case sensitive
console.log('juan' == juan);//true

// (===) Strict Equality Operator
console.log(" Strict Equality Operator");
console.log(1 === 1);//true
console.log(1 === 2);//false
console.log(1 === '1');//false - data type
console.log(0 === false); //false -number and a boolean
console.log('juan' === 'JUAN'); //false-case sensitive
console.log('juan' === juan); //true

// (!=) Inequality Operator
console.log("Inequality Operator");
console.log(1 != 1);//false
console.log(1 != 2);//true
console.log(1 != '1');//false
console.log(0 != false);//false
console.log('juan' != 'JUAN');//true - case sensitive
console.log('juan' != juan);//false

// (!==) Strict Inequality Operator
console.log(" Strict Inequality Operator");
console.log(1 !== 1);//false
console.log(1 !== 2);//true
console.log(1 !== '1');//true
console.log(0 !== false);//true
console.log('juan' !== 'JUAN');//true - case sensitive
console.log('juan' !== juan);//false

// Relational Comparision Operator
console.log	("Relational Comparison")
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// greater than(>)
console.log("Greater than:");
console.log(x > y);//false

// less than(<)
console.log("Less than:");
console.log(y<y);//false
console.log(numString < 6000); // True - force/type coercion to change string number
console.log(numString < 1000); // False

// Greater that or Equal to
console.log("Greater than or Equal to");
console.log(w >= w);//true

// Less that or Equal to
console.log("Less than or Equal to");
console.log(y <= y); //true

// Logical Operators (&&, ||, !)

let	isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

// Logical AND operator (&& - Double Ampersand)
console.log	("Logical AND Operator")
let	authorization1 = isAdmin && isRegistered;
console.log	(authorization1); //false

let	authorization2 = isAdmin && isRegistered;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25 &&isLegalAge;
console.log(authorization3); //true

// Logical OR Operator (|| - Double Pipe)
// return true if at least ONE of the operands are true.
console.log("OR Operator");

let userLevel1 = 100;
let userLevel2 = 65;

let userAge = 15;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel ||
userAge >= requiredAge;

console.log(guildRequirement1); //true

// NOT operator
console.log("Not Operator");
// turns a boolean into opposite value

let opposite = !isAdmin;
console.log(opposite); //true - isAdmin original value is false
console.log(!isRegistered);